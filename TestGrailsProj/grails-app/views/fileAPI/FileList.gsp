<%@ page import="testgrailsproj.APIController" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <link rel="stylesheet" href="${resource(dir: 'stylesheets', file: 'styles.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'stylesheets', file: 'ffolders.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'stylesheets', file: 'magnific-popup.css')}" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <asset:javascript src="jquery.magnific-popup.js"/>
    <asset:javascript src="script.js"/>
    <title>Black file system</title>
</head>
<body class="gradient-light--lean-left">
        <div class="row justify-content-center">
            <div class="col-10">
                <h3 class="mb-4 mt-5 text-align-right">תצוגה רכבה</h3>
                <div class="large-view-group">
                    <div class="row text-center flex-nowrap" style="height: 220px">
                        <% bigViewFilesList.each { file -> %>
                        <div class="col-6 col-xm-6 col-sm-6 col-md-3 col-lg-3 col-xl-2">
                            <a data-toggle="modal" data-target="#filePreviewModal" data-name="${file.name}" data-path="${file.getPath()}" data-link="${createLink(action:'getFileRowContent', controller:'API', params: [filePath: file.getPath()])}" data-assets-link="${createLink(action:'getPDFImages', controller:'API', params: [filePath: file.getPath()])}">
                                <div class="file_item shadow-sm shadow--on-hover">
                                    <img src="${createLink(action:'getFirstPagePreview', controller:'API', params: [filePath: file.getPath()])}" class="file_first_page"/>
                                    <div class="file_details_block">
                                        <span class="row justify-content-between align-items-center text-en">
                                            <span class="col-0 color--heading " style="position: absolute; margin-left: 17px">
                                                <span>
                                                    <img src="${createLink(action:'getFileFormatIcon', controller:'API', params: [fileName: file.name])}" class="file_format_icon" alt="file format"/>
                                                </span>
                                            </span>
                                            <span class="col dot-for-too-long-text">
                                                <div class="file_title">
                                                    ${file.name}
                                                </div>
                                            </span>
                                        </span>
                                        <p class="file_info_detail dot-for-too-long-text text-en">5 אזהרות | 9 עמודים</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <%}%>
                </div>
            </div>
        </div>
    </div>

    <div class="row text-align-right">
        %{--padding--}%
        <div class="col-1">
        </div>
        <div class="col-8">
            <h3 class="mb-1 mt-5 ">רשימת קבצים להשחרה</h3>
            <p class="mb-2 text-en">./${currPWD}</p>
            <div class="row row-grid">
                <div class="col-md-12 mb-2">
                    <div id="status-indicator-bar" data-sum-of-files="${new testgrailsproj.APIController().getNumOfFiles()}" style="height: 20px; width: 101%">
                        <div class="dot-for-too-long-text status-indicator-bar-item indicator-approved" data-toggle="tooltip"></div>
                        <div class="dot-for-too-long-text status-indicator-bar-item indicator-unapproved" data-toggle="tooltip" ></div>
                        <div class="dot-for-too-long-text status-indicator-bar-item indicator-unchecked" data-toggle="tooltip"></div>
                    </div>
                    %{--<div class="row justify-content">--}%
                        %{--<g:if test="${desirablePath.size() > 0}">--}%
                            %{--<f--}%
                                %{--<div class="ffolder small gray m-3">--}%
                                    %{--<p style="color: white; padding-top: 15px; font-size: 14px;">< Back</p>--}%
                                %{--</div>--}%
                            %{--</g:link>--}%
                        %{--</g:if>--}%
                        %{--<% directoriesList.eachWithIndex { currDirectory, dirIndex -> %>--}%
                            %{--<div class="flex-column">--}%
                                %{--<g:link controller="API" action="getFiles" params="[desirablePath: desirablePath, newPathIndex: dirIndex]" >--}%
                                    %{--<div class="ffolder small gray m-3">--}%
                                        %{--<p class="dir-name dot-for-too-long-text text-en">--}%
                                            %{--${currDirectory.name}--}%
                                        %{--</p>--}%
                                    %{--</div>--}%
                                %{--</g:link>--}%
                            %{--</div>--}%
                        %{--<%}%>--}%
                    %{--</div>--}%
                    <div class="job-list__wrapper mb-5 mt-3">
                        <g:if test="${desirablePath.size() > 0}">
                            <div class="card p-0 mb-3 border-0 shadow-sm shadow--on-hover pt-1 pb-1">
                                <g:link controller="API" action="getFiles" params="[desirablePath: desirablePath, newPathIndex: -1]" >
                                    <div class="card-body">
                                        <span class="row justify-content-between align-items-center">
                                            <span class="col-0 color--heading mr-2" style="position: absolute">
                                                <span>
                                                    <img src="${resource(dir: 'images', file: 'dir.png')}" class="file-type-for-files-list" alt="file format"/>
                                                </span>
                                            </span>
                                            <span class="col-4 dot-for-too-long-text text-en" style="margin-right: 33px; color: black;">
                                                <b>../</b>
                                            </span>
                                        </span>
                                    </div>
                                </g:link>
                            </div>
                        </g:if>
                        <% directoriesList.eachWithIndex { currDirectory, dirIndex -> %>
                            <div class="card p-0 mb-3 border-0 shadow-sm shadow--on-hover pt-1 pb-1">
                                <g:link controller="API" action="getFiles" params="[desirablePath: desirablePath, newPathIndex: dirIndex]" >
                                    <div class="card-body">
                                        <span class="row justify-content-between align-items-center">
                                            <span class="col-0 color--heading mr-2" style="position: absolute">
                                                <span>
                                                    <img src="${resource(dir: 'images', file: 'dir.png')}" class="file-type-for-files-list" alt="file format"/>
                                                </span>
                                            </span>
                                            <span class="col-8 dot-for-too-long-text text-en" style="margin-right: 33px; color: black;">
                                                <b>${currDirectory.name}</b>
                                            </span>

                                            %{--<span class="col-4 dot-for-too-long-text text-en" style="margin-right: 33px; color: black;">--}%
                                                %{--נבדקו 4/16 קבצים--}%
                                            %{--</span>--}%
                                        </span>
                                    </div>
                                </g:link>
                            </div>
                        <%}%>
                        <g:if test="${files.size() == 0}">
                            <div class="no-file-here-icon">
                                <svg style="flex: none; opacity: .5;" height="126" viewBox="0 0 150 150" width="130" focusable="false" role="presentation"><g fill="none" fill-rule="evenodd" stroke="none" stroke-width="1"><ellipse class="fill-color fill-opacity" cx="75" cy="142" fill="#0061d5" fill-opacity="0.1" rx="32.5" ry="3"></ellipse><path class="fill-color" d="M97,6.1337822 L97,5.49178758 C97,5.2201808 97.2319336,5 97.5,5 C97.7761424,5 98,5.21505737 98,5.49047852 L98,6.1337822 C98.151814,6.22160185 98.2783981,6.348186 98.3662178,6.5 L99.0095215,6.5 C99.2804053,6.5 99.5,6.73193359 99.5,7 C99.5,7.27614237 99.2849426,7.5 99.0095215,7.5 L98.3662178,7.5 C98.2783981,7.651814 98.151814,7.77839815 98,7.8662178 L98,8.50952148 C98,8.78040529 97.7680664,9 97.5,9 C97.2238576,9 97,8.76897915 97,8.49525623 L97,7.8662178 C96.848186,7.77839815 96.7216019,7.651814 96.6337822,7.5 L95.9904785,7.5 C95.7195947,7.5 95.5,7.26806641 95.5,7 C95.5,6.72385763 95.7150574,6.5 95.9904785,6.5 L96.6337822,6.5 C96.7216019,6.348186 96.848186,6.22160185 97,6.1337822 Z M9.5,89.6337822 L9.5,88.9917876 C9.5,88.7201808 9.73193359,88.5 10,88.5 C10.2761424,88.5 10.5,88.7150574 10.5,88.9904785 L10.5,89.6337822 C10.651814,89.7216019 10.7783981,89.848186 10.8662178,90 L11.5095215,90 C11.7804053,90 12,90.2319336 12,90.5 C12,90.7761424 11.7849426,91 11.5095215,91 L10.8662178,91 C10.7783981,91.151814 10.651814,91.2783981 10.5,91.3662178 L10.5,92.0095215 C10.5,92.2804053 10.2680664,92.5 10,92.5 C9.72385763,92.5 9.5,92.2689791 9.5,91.9952562 L9.5,91.3662178 C9.348186,91.2783981 9.22160185,91.151814 9.1337822,91 L8.49047852,91 C8.21959471,91 8,90.7680664 8,90.5 C8,90.2238576 8.21505737,90 8.49047852,90 L9.1337822,90 C9.22160185,89.848186 9.348186,89.7216019 9.5,89.6337822 Z M137.5,84.6337822 L137.5,83.9917876 C137.5,83.7201808 137.731934,83.5 138,83.5 C138.276142,83.5 138.5,83.7150574 138.5,83.9904785 L138.5,84.6337822 C138.651814,84.7216019 138.778398,84.848186 138.866218,85 L139.509521,85 C139.780405,85 140,85.2319336 140,85.5 C140,85.7761424 139.784943,86 139.509521,86 L138.866218,86 C138.778398,86.151814 138.651814,86.2783981 138.5,86.3662178 L138.5,87.0095215 C138.5,87.2804053 138.268066,87.5 138,87.5 C137.723858,87.5 137.5,87.2689791 137.5,86.9952562 L137.5,86.3662178 C137.348186,86.2783981 137.221602,86.151814 137.133782,86 L136.490479,86 C136.219595,86 136,85.7680664 136,85.5 C136,85.2238576 136.215057,85 136.490479,85 L137.133782,85 C137.221602,84.848186 137.348186,84.7216019 137.5,84.6337822 Z M113.5,23.1337822 L113.5,22.4917876 C113.5,22.2201808 113.731934,22 114,22 C114.276142,22 114.5,22.2150574 114.5,22.4904785 L114.5,23.1337822 C114.651814,23.2216019 114.778398,23.348186 114.866218,23.5 L115.509521,23.5 C115.780405,23.5 116,23.7319336 116,24 C116,24.2761424 115.784943,24.5 115.509521,24.5 L114.866218,24.5 C114.778398,24.651814 114.651814,24.7783981 114.5,24.8662178 L114.5,25.5095215 C114.5,25.7804053 114.268066,26 114,26 C113.723858,26 113.5,25.7689791 113.5,25.4952562 L113.5,24.8662178 C113.348186,24.7783981 113.221602,24.651814 113.133782,24.5 L112.490479,24.5 C112.219595,24.5 112,24.2680664 112,24 C112,23.7238576 112.215057,23.5 112.490479,23.5 L113.133782,23.5 C113.221602,23.348186 113.348186,23.2216019 113.5,23.1337822 Z M33.5,20.6337822 L33.5,19.9917876 C33.5,19.7201808 33.7319336,19.5 34,19.5 C34.2761424,19.5 34.5,19.7150574 34.5,19.9904785 L34.5,20.6337822 C34.651814,20.7216019 34.7783981,20.848186 34.8662178,21 L35.5095215,21 C35.7804053,21 36,21.2319336 36,21.5 C36,21.7761424 35.7849426,22 35.5095215,22 L34.8662178,22 C34.7783981,22.151814 34.651814,22.2783981 34.5,22.3662178 L34.5,23.0095215 C34.5,23.2804053 34.2680664,23.5 34,23.5 C33.7238576,23.5 33.5,23.2689791 33.5,22.9952562 L33.5,22.3662178 C33.348186,22.2783981 33.2216019,22.151814 33.1337822,22 L32.4904785,22 C32.2195947,22 32,21.7680664 32,21.5 C32,21.2238576 32.2150574,21 32.4904785,21 L33.1337822,21 C33.2216019,20.848186 33.348186,20.7216019 33.5,20.6337822 Z M132.5,52.6337822 L132.5,51.9917876 C132.5,51.7201808 132.731934,51.5 133,51.5 C133.276142,51.5 133.5,51.7150574 133.5,51.9904785 L133.5,52.6337822 C133.651814,52.7216019 133.778398,52.848186 133.866218,53 L134.509521,53 C134.780405,53 135,53.2319336 135,53.5 C135,53.7761424 134.784943,54 134.509521,54 L133.866218,54 C133.778398,54.151814 133.651814,54.2783981 133.5,54.3662178 L133.5,55.0095215 C133.5,55.2804053 133.268066,55.5 133,55.5 C132.723858,55.5 132.5,55.2689791 132.5,54.9952562 L132.5,54.3662178 C132.348186,54.2783981 132.221602,54.151814 132.133782,54 L131.490479,54 C131.219595,54 131,53.7680664 131,53.5 C131,53.2238576 131.215057,53 131.490479,53 L132.133782,53 C132.221602,52.848186 132.348186,52.7216019 132.5,52.6337822 Z M27.9375,28.7086139 L27.9375,28.3073672 C27.9375,28.137613 28.0824585,28 28.25,28 C28.422589,28 28.5625,28.1344109 28.5625,28.3065491 L28.5625,28.7086139 C28.6573838,28.7635012 28.7364988,28.8426162 28.7913861,28.9375 L29.1934509,28.9375 C29.3627533,28.9375 29.5,29.0824585 29.5,29.25 C29.5,29.422589 29.3655891,29.5625 29.1934509,29.5625 L28.7913861,29.5625 C28.7364988,29.6573838 28.6573838,29.7364988 28.5625,29.7913861 L28.5625,30.1934509 C28.5625,30.3627533 28.4175415,30.5 28.25,30.5 C28.077411,30.5 27.9375,30.355612 27.9375,30.1974433 L27.9375,29.7913861 C27.8426162,29.7364988 27.7635012,29.6573838 27.7086139,29.5625 L27.3065491,29.5625 C27.1372467,29.5625 27,29.4175415 27,29.25 C27,29.077411 27.1344109,28.9375 27.3065491,28.9375 L27.7086139,28.9375 C27.7635012,28.8426162 27.8426162,28.7635012 27.9375,28.7086139 Z M16.4375,83.7086139 L16.4375,83.3073672 C16.4375,83.137613 16.5824585,83 16.75,83 C16.922589,83 17.0625,83.1344109 17.0625,83.3065491 L17.0625,83.7086139 C17.1573838,83.7635012 17.2364988,83.8426162 17.2913861,83.9375 L17.6934509,83.9375 C17.8627533,83.9375 18,84.0824585 18,84.25 C18,84.422589 17.8655891,84.5625 17.6934509,84.5625 L17.2913861,84.5625 C17.2364988,84.6573838 17.1573838,84.7364988 17.0625,84.7913861 L17.0625,85.1934509 C17.0625,85.3627533 16.9175415,85.5 16.75,85.5 C16.577411,85.5 16.4375,85.355612 16.4375,85.1974433 L16.4375,84.7913861 C16.3426162,84.7364988 16.2635012,84.6573838 16.2086139,84.5625 L15.8065491,84.5625 C15.6372467,84.5625 15.5,84.4175415 15.5,84.25 C15.5,84.077411 15.6344109,83.9375 15.8065491,83.9375 L16.2086139,83.9375 C16.2635012,83.8426162 16.3426162,83.7635012 16.4375,83.7086139 Z M123.4375,48.2086139 L123.4375,47.8073672 C123.4375,47.637613 123.582458,47.5 123.75,47.5 C123.922589,47.5 124.0625,47.6344109 124.0625,47.8065491 L124.0625,48.2086139 C124.157384,48.2635012 124.236499,48.3426162 124.291386,48.4375 L124.693451,48.4375 C124.862753,48.4375 125,48.5824585 125,48.75 C125,48.922589 124.865589,49.0625 124.693451,49.0625 L124.291386,49.0625 C124.236499,49.1573838 124.157384,49.2364988 124.0625,49.2913861 L124.0625,49.6934509 C124.0625,49.8627533 123.917542,50 123.75,50 C123.577411,50 123.4375,49.855612 123.4375,49.6974433 L123.4375,49.2913861 C123.342616,49.2364988 123.263501,49.1573838 123.208614,49.0625 L122.806549,49.0625 C122.637247,49.0625 122.5,48.9175415 122.5,48.75 C122.5,48.577411 122.634411,48.4375 122.806549,48.4375 L123.208614,48.4375 C123.263501,48.3426162 123.342616,48.2635012 123.4375,48.2086139 Z" fill="#0061d5"></path><path class="stroke-color" d="M68.8246342,87.5 C76.40963,91.8231286 93.0935221,92.3842512 94.8471309,98.1662993 C96.6007397,103.948347 51.5,108.892186 51.5,108.892186 L56.3324509,114.952313" stroke="#0061d5" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path><path class="stroke-color" d="M109.403973,65.6681213 C121.308377,94.6323489 136.937803,71.8237566 136.937803,71.8237566" stroke="#0061d5" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path><path class="stroke-color" d="M135,69.348975 C137.383789,74.3303392 145.105602,69 145.105602,69" stroke="#0061d5" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path><path class="stroke-color" d="M81.0723406,87.5 C73.4873447,91.8231286 56.8034526,92.3842512 55.0498438,98.1662993 C53.296235,103.948347 98.3969748,108.892186 98.3969748,108.892186 L93.5645239,114.952313" stroke="#0061d5" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path><path class="stroke-color" d="M13.0621966,65.6681213 C24.9666013,94.6323489 40.5960273,71.8237566 40.5960273,71.8237566" stroke="#0061d5" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" transform="translate(26.829112, 73.012365) scale(-1, 1) translate(-26.829112, -73.012365) "></path><path class="stroke-color" d="M5,69.348975 C7.38378882,74.3303392 15.1056022,69 15.1056022,69" stroke="#0061d5" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" transform="translate(10.052801, 70.237963) scale(-1, 1) translate(-10.052801, -70.237963) "></path><path class="stroke-color fill-white" d="M35,37.9900737 C35,37.1671287 35.6677833,36.5 36.5086652,36.5 L64.7706257,36.5 C65.6038385,36.5 66.5588137,37.1050351 66.9100361,37.8652661 L70.1170158,44.806865 C70.4653667,45.5608807 70.0812601,46.1721311 69.2572363,46.1721311 L36.4905247,46.1721311 C35.6673306,46.1721311 35,45.5073264 35,44.6820574 L35,37.9900737 Z" fill="#FFFFFF" stroke="#0061d5" stroke-width="2"></path><path class="fill-color fill-opacity" d="M36.7873881,39.9245 C36.7873881,39.101555 37.4555019,38.4344262 38.2810514,38.4344262 L63.4450865,38.4344262 C64.270014,38.4344262 65.1964383,39.054175 65.5149504,39.8202068 L68.3841724,46.7207768 C68.7023992,47.4861223 68.2883944,48.1065574 67.4517556,48.1065574 L38.2960054,48.1065574 C37.462819,48.1065574 36.7873881,47.4417526 36.7873881,46.6164836 L36.7873881,39.9245 Z" fill="#0061d5" fill-opacity="0.1"></path><rect class="stroke-color fill-white" fill="#FFFFFF" height="53.1967213" rx="1.5" stroke="#0061d5" stroke-width="2" width="80" x="35" y="42.3032787"></rect><path class="fill-color fill-opacity" d="M37,81.7185235 C37,80.8915052 37.6574211,80.3784293 38.4748917,80.5333945 C38.4748917,80.5333945 42.0893396,81.8018989 62.3333333,81.8018989 C74.9176315,81.8018989 80.4976661,85.1139445 87.6666667,85.1139445 C102.165889,85.1139445 111.621106,80.7823489 111.621106,80.7823489 C112.382648,80.4723656 113,80.8978342 113,81.7185235 L113,92.0025509 C113,92.8295692 112.319761,93.5 111.498955,93.5 L38.5010449,93.5 C37.6720407,93.5 37,92.8232402 37,92.0025509 L37,81.7185235 Z" fill="#0061d5" fill-opacity="0.1"></path></g></svg>
                                <p>
                                    אין קבצים בתקייה זו.
                                </p>
                            </div>
                        </g:if>
                        <% files.eachWithIndex {file, currFileIndex -> %>
                            <a data-toggle="modal" data-target="#filePreviewModal" data-id="fileId-${file.id}" data-name="${file.name}" data-path="${file.getPath()}" data-link="${createLink(action:'getFileRowContent', controller:'API', params: [filePath: file.getPath()])}" data-assets-link="${createLink(action:'getPDFImages', controller:'API', params: [filePath: file.getPath()])}" class="fileInFilesList card p-0 mb-3 border-0 shadow-sm shadow--on-hover">
                                <div class="card-body">
                                    <span class="row justify-content-between align-items-center">
                                        <span class="col-0 color--heading mr-2" style="position: absolute">
                                            <span>
                                                 <img src="${createLink(action:'getFileFormatIcon', controller:'API', params: [fileName: file.name])}" class="file-type-for-files-list" alt="file format"/>
                                            </span>
                                        </span>
                                        <span class="col-4 dot-for-too-long-text text-en" style="margin-right: 33px">
                                            <b>${file.name}</b>
                                        </span>
                                        <span class="col-5 dot-for-too-long-text text-en">
                                            ${file.getPath()}
                                        </span>
                                        <span class="col-2">
                                            <div class="file-check-status dot-for-too-long-text uncheck">
                                                <p>תרם נבדק</p>
                                            </div>
                                            %{--<g:if test="${currFileIndex % 4 == 0 }">--}%
                                                %{--<div class="file-check-status dot-for-too-long-text unapproved">--}%
                                                    %{--<p>לא אושר</p>--}%
                                                %{--</div>--}%
                                            %{--</g:if>--}%
                                            %{--<g:elseif test="${currFileIndex == 6 || currFileIndex == 7 || currFileIndex == 11 || currFileIndex == 1 || currFileIndex == 3}">--}%
                                                %{--<div class="file-check-status dot-for-too-long-text uncheck">--}%
                                                    %{--<p>תרם נבדק</p>--}%
                                                %{--</div>--}%
                                            %{--</g:elseif>--}%
                                            %{--<g:else>--}%
                                                %{--<div class="file-check-status dot-for-too-long-text approved">--}%
                                                    %{--<p>אושר</p>--}%
                                                %{--</div>--}%
                                            %{--</g:else>--}%
                                        </span>
                                    </span>
                                </div>
                            </a>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2 mt-5">
            <h3 class="mb-4">פיצ׳ר מגניב</h3>
            <div class="unapproved-files shadow-sm">
                <div style="padding: 10px">
                    <div class="row">
                    <% bigViewFilesList.each { file -> %>
                        <div class="col">
                        <a data-toggle="modal" data-target="#filePreviewModal" data-name="${file.name}" data-path="${file.getPath()}" data-link="${createLink(action:'getFileRowContent', controller:'API', params: [filePath: file.getPath()])}" data-assets-link="${createLink(action:'getPDFImages', controller:'API', params: [filePath: file.getPath()])}">
                            <div class="one-unapproved-file dot-for-too-long-text text-align-left text-en">
                                ${file.name}
                            </div>
                            </a>
                        </div>
                    <%}%>
                    </div>
                </div>
            </div>

            <h4 class="mb-3 mt-5">סנן לפי</h4>
            <form class="filter-form">
                <div class="mb-3">
                    <div class="sort-options">
                        <select id="sort_oprtion" class="custom-select">
                            <option value="position1">הכי הרבה אזהרות קודם</option>
                            <option value="position2">הכי הרבה התראות בסוף</option>
                            <option value="position3">לפי גודל</option>
                            <option value="position4">לפי סוג קובץ</option>
                            <option value="position5">קבצים שלא נבדקו קודם</option>
                            <option value="position6">קבצים שנבדקו</option>
                            <option value="position7">קבצים שלא נבדקו</option>
                        </select>
                    </div>
                </div>
            </form>
            <div class="dot-for-too-long-text finish-button mt-5">
                <p><b>סיים מעבר</b></p>
            </div>
        </div>
    </div>

    <div class="modal fade" id="filePreviewModal" tabindex="-1" role="dialog" aria-labelledby="filePreviewModalTitle" aria-hidden="true" style="height: 95%; z-index: 1041; direction: ltr">
        <div class="row justify-content-end">
            <div class="col-6">
                <div class="modal-dialog mw-100" role="document" style="height: 87%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="filePreviewModalTitle">Have no name</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        </div>
                        <div class="modal-footer">
                            %{--<img src="${resource(dir: 'images', file: 'download.png')}" style="width: 25px" alt="download image"/>--}%
                            <button type="button" class="btn btn-primary">הורדה</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">סיים בדיקת קובץ</button>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-5" style="color:white; margin-top: 1.75rem;">
                <div style="width: 95%">
                    <h2 id="modalMoreDetailsFileName" class="dot-for-too-long-text"></h2>
                    <p id="modalMoreDetailsFilePath" class="dot-for-too-long-text"></p>
                    <p id="modalMoreDetailsSHA" class="dot-for-too-long-text"></p>
                    <p id="modalMoreDetailsNumberOfWarnings" class="dot-for-too-long-text"></p>
                    <p id="modalMoreDetailsCreationDate" class="dot-for-too-long-text"></p>

                    <div class="row gallery">
%{--                        <div class="col-lg-3 col-md-4 col-xs-6 thumb">--}%
%{--                            <a href="https://picsum.photos/940/650?random=1">--}%
%{--                                <img class="img-fluid img-thumbnail" src="https://picsum.photos/940/650?random=1" alt="Random Image">--}%
%{--                            </a>--}%
%{--                        </div>--}%

%{--                        <div class="col-lg-3 col-md-4 col-xs-6 thumb">--}%
%{--                            <a href="https://picsum.photos/940/650?random=5">--}%
%{--                                <img class="img-fluid img-thumbnail" src="https://picsum.photos/940/650?random=5" alt="Random Image">--}%
%{--                            </a>--}%
%{--                        </div>--}%

%{--                        <div class="col-lg-3 col-md-4 col-xs-6 thumb">--}%
%{--                            <a href="https://picsum.photos/940/650?random=6">--}%
%{--                                <img class="img-fluid img-thumbnail" src="https://picsum.photos/940/650?random=6" alt="Random Image">--}%
%{--                            </a>--}%
%{--                        </div>--}%

%{--                        <div class="col-lg-3 col-md-4 col-xs-6 thumb">--}%
%{--                            <a href="https://picsum.photos/940/650?random=7">--}%
%{--                                <img class="img-fluid img-thumbnail" src="https://picsum.photos/940/650?random=7" alt="Random Image">--}%
%{--                            </a>--}%
%{--                        </div>--}%

%{--                        <div class="col-lg-3 col-md-4 col-xs-6 thumb">--}%
%{--                            <a href="https://picsum.photos/940/650?random=8">--}%
%{--                                <img class="img-fluid img-thumbnail" src="https://picsum.photos/940/650?random=8" alt="Random Image">--}%
%{--                            </a>--}%
%{--                        </div>--}%
                    </div>

                    <div class="file-actions">
                        <div class="d-flex justify-content-center">
                            <div class="p-2">
                                <a id="unapproved-this-file-button" class="modal-action-button unapproved-this-file-button shadow-sm">
                                    דחה קובץ
                                </a>
                            </div>
                            <div class="p-2">
                                <div class="d-flex justify-content-center">
                                    <textarea class="add_command" placeholder="הוסף הערה..."></textarea>
                                </div>
                            </div>
                            <div class="p-2">
                                <a id="approved-this-file-button" class="modal-action-button shadow-sm approved-this-file-button">
                                        אשר קובץ
                                </a>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center">
                            <div id="file-status-in-preview" class="file-status-in-preview uncheck">
                                תרם נבדק
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>