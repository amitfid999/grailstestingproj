package testgrailsproj

import groovy.io.FileType
import org.faceless.pdf2.*

import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import java.awt.image.ColorModel
import java.awt.image.RenderedImage
import java.awt.image.WritableRaster

class APIController {

    def getFiles() {
        File dir = new File("${params.baseFolder}")
        def newPathIndex = params.newPathIndex
        def desirablePath = params.desirablePath

        if(desirablePath == null){
            return false
        }
        if(newPathIndex != null){
            desirablePath = setNewDesirablePath(Integer.parseInt(newPathIndex), desirablePath)
        }

        def filesList = []
        def directoriesList = []

        desirablePath.each { currDesirablePathIndex ->
            directoriesList = []
            dir.eachFile{ File currFile ->
                if(currFile.isDirectory()) {
                    directoriesList.add(currFile)
                }
            }
            if(directoriesList.size() > currDesirablePathIndex && currDesirablePathIndex >= 0){
                dir = directoriesList[currDesirablePathIndex]
            }
        }
        directoriesList = []
        def indexOfFile = 0
        dir.eachFile{ File currFile ->
            if(currFile.isDirectory()) {
                directoriesList.add(currFile)
            }
            else {
                currFile.metaClass.id = getFileId(desirablePath, indexOfFile++)
                filesList.add(currFile)
            }
        }

        // get top files
        def bigViewFilesList = []
        int maxLargeViewFiles = 10
        int i = 0
        dir.eachFile { File currFile ->
            if(!currFile.isDirectory()) {
                if (maxLargeViewFiles > i) {
                    bigViewFilesList.add(currFile)
                }
                i ++
            }
        }

        render(view: "/fileAPI/FileList", model: [files: filesList, bigViewFilesList: bigViewFilesList, directoriesList: directoriesList, currPWD: dir.getPath(), desirablePath: desirablePath])
    }

    def getNumOfFiles(){
        int numOfFiles = 0;
        new File("FileFolder").eachFileRecurse (FileType.FILES) { file ->
            if(file.isFile()){
                numOfFiles ++
            }
        }
        return numOfFiles
//        File("FileFolder").listFiles().findAll { it.isFile() }.size()
    }

    // Getting images from pdf with BFO PDF library
    def getPDFImages(){
        // here is how to open the file.
        InputStream inputStream = new FileInputStream(params.filePath);
        PDFReader reader = new PDFReader(inputStream);
        PDF pdf = new PDF(reader);
        PDFParser parser = new PDFParser(pdf);

        // get a list of all the pages
        List<PDFPage> pages = pdf.getPages();
        // create a new list to hold the images we find
        List<PageExtractor.Image> allImgs = new ArrayList<>();

        pages.each({ page ->
            // use the parser to get a PageExtractor
            PageExtractor extract = parser.getPageExtractor(page);
            Collection<PageExtractor.Image> imgs = extract.getImages();
            allImgs.addAll(imgs);
        })
        // we now have a list of all the images that were found in the file!

        List<byte[]> imgs = []

        allImgs.each({i ->
            PageExtractor.Image bfoImg = i
            // first step get the RenderedImage object from the BFO Image.
            RenderedImage img = bfoImg.getImage()
            BufferedImage bImage = convertRenderedImage(img)
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(bImage, "png", bos);
            byte [] data = bos.toByteArray();
            imgs.add(data)
        })
        return imgs
    }

    // temp helping method
    private static BufferedImage convertRenderedImage(RenderedImage img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }
        ColorModel cm = img.getColorModel();
        int width = img.getWidth();
        int height = img.getHeight();
        WritableRaster raster = cm
                .createCompatibleWritableRaster(width, height);
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        Hashtable properties = new Hashtable();
        String[] keys = img.getPropertyNames();
        if (keys != null) {
            for (int i = 0; i < keys.length; i++) {
                properties.put(keys[i], img.getProperty(keys[i]));
            }
        }
        BufferedImage result = new BufferedImage(cm, raster,
                isAlphaPremultiplied, properties);
        img.copyData(raster);
        return result;
    }

    def getFileId(desirablePath, fileIndex){
        StringBuilder fileId = new StringBuilder()
        desirablePath.each { currDirIndex ->
            fileId.append(currDirIndex.toString()).append('-')
        }
        fileId.append(fileIndex.toString())
        return fileId
    }

    def setNewDesirablePath(newPathIndex, desirablePath){
        if(newPathIndex == -1){
            if(desirablePath.size() > 0){
                desirablePath.removeLast()
            }
        }
        else if(newPathIndex >= 0){
            desirablePath.add(newPathIndex)
        }

        return desirablePath
    }
    def downloadPdf() {
        File fileToReturn = new File("${params.f}")
        String fileName = fileToReturn.name
        def byteOutput = fileToReturn.readBytes()
        response.setHeader("Content-disposition", "attachment; filename=\"${fileName}\"")
//        response.contentType = 'application/pdf'
        response.outputStream << byteOutput
    }

    def getFileRowContent() {
        File fileToReturn = new File("${params.filePath}")
        def byteOutput = fileToReturn.readBytes()
        response.setHeader("Content-disposition", "attachment; filename=\"${fileToReturn.name}\"")
        response.outputStream << byteOutput
    }

    def getFirstPagePreview() {
        File fileToReturn = new File("${params.filePath}")
        String fileName = fileToReturn.name
        def byteOutput

        if((fileName =~ /.*png$/).find()){
        }
        else if((fileName =~ /.*jpg$/).find()){
        }

        else if((fileName =~ /.*pdf$/).find()){
            fileToReturn = new File("./grails-app/assets/images/pdf.png")
        }
        else if((fileName =~ /.*mp3$/).find()){
            fileToReturn = new File("./grails-app/assets/images/mp3.png")
        }
        else if((fileName =~ /.*txt$/).find()){
            fileToReturn = new File("./grails-app/assets/images/txt.png")
        }
        else if((fileName =~ /.*html$/).find()){
            fileToReturn = new File("./grails-app/assets/images/html.png")
        }
        else if((fileName =~ /.*doc$/).find()){
            fileToReturn = new File("./grails-app/assets/images/doc.png")
        }
        else if((fileName =~ /.*docx$/).find()){
            fileToReturn = new File("./grails-app/assets/images/docx.png")
        }
        else if((fileName =~ /.*ppt$/).find()){
            fileToReturn = new File("./grails-app/assets/images/ppt.png")
        }
        else {
            fileToReturn = new File("./grails-app/assets/images/pdf.png")

        }
        byteOutput = fileToReturn.readBytes()
        response.outputStream << byteOutput
    }

    def getFileFormatIcon(){
        def fileName = params.fileName
        File fileFormatIcon

        if((fileName =~ /.*doc$/).find()){
            fileFormatIcon = new File("./grails-app/assets/images/doc.png")
        }
        else if((fileName =~ /.*docx$/).find()){
            fileFormatIcon = new File("./grails-app/assets/images/docx.png")
        }
        else if((fileName =~ /.*ppt$/).find()){
            fileFormatIcon = new File("./grails-app/assets/images/ppt.png")
        }
        else if((fileName =~ /.*jpg$/).find()){
            fileFormatIcon = new File("./grails-app/assets/images/jpg.png")
        }
        else if((fileName =~ /.*pdf$/).find()){
            fileFormatIcon = new File("./grails-app/assets/images/pdf.png")
        }
        else if((fileName =~ /.*mp3$/).find()){
            fileFormatIcon = new File("./grails-app/assets/images/mp3.png")
        }
        else if((fileName =~ /.*html$/).find()){
            fileFormatIcon = new File("./grails-app/assets/images/html.png")
        }
        else if((fileName =~ /.*xml$/).find()){
            fileFormatIcon = new File("./grails-app/assets/images/xml.png")
        }
        else if((fileName =~ /.*zip$/).find()){
            fileFormatIcon = new File("./grails-app/assets/images/zip.png")
        }
        else {
            fileFormatIcon = new File("./grails-app/assets/images/txt.png")
        }

        def byteOutput = fileFormatIcon.readBytes()
        response.outputStream << byteOutput
        response.outputStream.flush()
    }
}
 