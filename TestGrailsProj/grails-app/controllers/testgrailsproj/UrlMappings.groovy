package testgrailsproj

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "API", action: "getFiles"){
            baseFolder = "FileFolder"
            desirablePath = []
        }
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
