$(document).ready(function() {
    $(".gallery").magnificPopup({
        delegate: "a",
        type: "image",
        tLoading: "Loading image #%curr%...",
        mainClass: "mfp-img-mobile",
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
        }
    });
    $(".gallery").magif

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    updateStatusIndicatorBar();
    function updateStatusIndicatorBar(){
        var sumOfFiles = $("#status-indicator-bar").data("sum-of-files");
        var partOfApprovedFiles = Math.floor((getNumOfItemsInLocalStorageList('approved-files-list') / sumOfFiles) * 100);
        var partOfUnapprovedFiles = Math.floor((getNumOfItemsInLocalStorageList('unapproved-files-list') / sumOfFiles) * 100);
        var partOfUncheckedFiles = Math.floor(100 - partOfApprovedFiles - partOfUnapprovedFiles - 1);

        $(".indicator-approved").css("width", partOfApprovedFiles.toString() + "%");
        $(".indicator-unapproved").css("width", partOfUnapprovedFiles.toString() + "%");
        $(".indicator-unchecked").css("width", partOfUncheckedFiles.toString() + "%");

        $('.indicator-approved').attr('data-original-title', partOfApprovedFiles.toString() + " קבצים שאושרו");
        $('.indicator-unapproved').attr('data-original-title', partOfUnapprovedFiles.toString() + " קבצים שלא אושרו");
        $('.indicator-unchecked').attr('data-original-title', partOfUncheckedFiles.toString() + " קבצים שתרם נבדקו");

    }

    updateAllFilesStatuses();
    function updateAllFilesStatuses(){
        var currFileId;
        $(".fileInFilesList").each(function() {
            currFileId = $(this).data()["id"];
            $(this).find(".file-check-status").removeClass("uncheck", "unapproved", "approved");
            if(isItemExistInLocalStorageList('unapproved-files-list', currFileId)){
                $(this).find(".file-check-status").addClass("unapproved");
            }
            else if (isItemExistInLocalStorageList('approved-files-list', currFileId)){
                $(this).find(".file-check-status").addClass("approved");
            }
            else {
                $(this).find(".file-check-status").addClass("uncheck");
            }
        });
    }

    function updateActionButtonInPreview(fileId){
        $("#approved-this-file-button").removeClass("approved-this-file-button");
        $("#approved-this-file-button").removeClass("approved-this-file-button-after-click");
        $("#unapproved-this-file-button").removeClass("unapproved-this-file-button");
        $("#unapproved-this-file-button").removeClass("unapproved-this-file-button-after-click");

        if(isItemExistInLocalStorageList('unapproved-files-list', fileId)){
            unapprovedStateForActionButton();
        }
        else if (isItemExistInLocalStorageList('approved-files-list', fileId)){
            approvedStateForActionButton();
        }
        else {
            uncheckedStateForActionButton();
        }
    }

    function unapprovedStateForActionButton(){
        setPreviewActionButton("#approved-this-file-button", "approved-this-file-button", "אשר קובץ");
        setPreviewActionButton("#unapproved-this-file-button", "unapproved-this-file-button-after-click", "קובץ נדחה");
        setPreviewFileStatus("קובץ נדחה", "unapproved");
    }

    function approvedStateForActionButton(){
        setPreviewActionButton("#approved-this-file-button", "approved-this-file-button-after-click", "קובץ אושר");
        setPreviewActionButton("#unapproved-this-file-button", "unapproved-this-file-button", "דחה קובץ");
        setPreviewFileStatus("קובץ אושר", "approved");
    }

    function uncheckedStateForActionButton(){
        setPreviewActionButton("#approved-this-file-button", "approved-this-file-button", "אשר קובץ");
        setPreviewActionButton("#unapproved-this-file-button", "unapproved-this-file-button", "דחה קובץ");
        setPreviewFileStatus("תרם נבדק", "uncheck");
    }


    function addItemToLocalStorageList(localStorageItemName, itemToAdd){
        var currList = localStorage.getItem(localStorageItemName);
        if(currList == null){
            currList = [itemToAdd];
            localStorage.setItem(localStorageItemName, JSON.stringify(currList));
        }
        else {
            currList = JSON.parse(currList);
            currList.push(itemToAdd);
            localStorage.setItem(localStorageItemName, JSON.stringify(currList));
        }
    }

    function removeItemFromLocalStorage(localStorageItemName, itemToRemove){
        var currList = localStorage.getItem(localStorageItemName);
        if(currList != null){
            currList = JSON.parse(currList);
            currList = currList.filter(function(value, index, arr){
                return value != itemToRemove;
            });
            localStorage.setItem(localStorageItemName, JSON.stringify(currList));
        }
    }

    function isItemExistInLocalStorageList(localStorageListName, item){
        var currList = localStorage.getItem(localStorageListName);
        if(currList != null){
            currList = JSON.parse(currList);
            return currList.includes(item);
        }
        return false;
    }

    function getNumOfItemsInLocalStorageList(localStorageListName){
        var currList = localStorage.getItem(localStorageListName);
        if(currList != null){
            currList = JSON.parse(currList);
            return currList.length;
        }
        return 0;
    }

    function setPreviewActionButton(actionButtonId, classToAdd , text) {
        $(actionButtonId).removeClass("approved-this-file-button-after-click");
        $(actionButtonId).removeClass("approved-this-file-button");
        $(actionButtonId).removeClass("unapproved-this-file-button");
        $(actionButtonId).removeClass("unapproved-this-file-button-after-click");
        $(actionButtonId).addClass(classToAdd);
        $(actionButtonId).text(text);
    }

    function setPreviewFileStatus(text, addedClass){
        $(".file-status-in-preview").removeClass("uncheck");
        $(".file-status-in-preview").removeClass("approved");
        $(".file-status-in-preview").removeClass("unapproved");
        $(".file-status-in-preview").addClass(addedClass);
        $(".file-status-in-preview").empty();
        $(".file-status-in-preview").text(text)
    }


    $(document).on('click', '.approved-this-file-button', function(e){
        var fileId = $("#approved-this-file-button").data("file-id");
        addItemToLocalStorageList('approved-files-list', fileId);

        approvedStateForActionButton();

        removeItemFromLocalStorage('unapproved-files-list', fileId);
        updateAllFilesStatuses();
        updateStatusIndicatorBar();
    });

    $(document).on('click', '.unapproved-this-file-button', function(e){
        var fileId = $("#unapproved-this-file-button").data("file-id");
        addItemToLocalStorageList('unapproved-files-list', fileId);

        unapprovedStateForActionButton();

        removeItemFromLocalStorage('approved-files-list', fileId);
        updateAllFilesStatuses();
        updateStatusIndicatorBar();
    });


    $('#filePreviewModal').on('show.bs.modal', function (event) {
        var file_item = $(event.relatedTarget); // Button that triggered the modal
        var file_name = file_item.data('name'); // Extract info from data-* attributes
        var file_link = file_item.data('link');
        var file_path = file_item.data('path');
        var file_ID = file_item.data('id');
        var file_assets_link = file_item.data('assets-link');


        var modal = $(this);
        modal.find('.modal-body').empty();
        modal.find('.modal-title').text(file_name);

        jQuery.removeData( $("#approved-this-file-button" )[0], "file-id");
        jQuery.removeData( $("#unapproved-this-file-button" )[0], "file-id" );
        $("#approved-this-file-button").data("file-id", file_ID );
        $("#unapproved-this-file-button").data("file-id", file_ID);

        updateActionButtonInPreview(file_ID);

        $('#modalMoreDetailsFileName').text(file_name);
        $('#modalMoreDetailsFilePath').text("File path : " + file_path);
        $('#modalMoreDetailsSHA').text("SHA256 : " + "688787d8ff144c502c7f5cffaafe2cc588d86079f9de88304c26b0cb99ce91c6");
        $('#modalMoreDetailsNumberOfWarnings').text("Number of warnings : " + "7");
        $('#modalMoreDetailsCreationDate').text("Created : " + "27/2/2019 14:44 PM");

        var xhr = new XMLHttpRequest();
        xhr.open('GET', file_link, true);
        xhr.responseType = 'blob';

        if(/.*png$/.test(file_name)){
            modal.find('.modal-body').prepend("<img style='width: 95%' src='"+ file_link + "'></img>")
        }
        else if(/.*jpg/.test(file_name)){
            modal.find('.modal-body').prepend("<img style='width: 95%' src='"+ file_link + "'></img>")
        }
        else if(/.*pdf/.test(file_name)){
            xhr.onload = function(e) {
                if (this.status == 200) {
                    // Note: .response instead of .responseText
                    var blob = new Blob([this.response], {type: 'application/pdf'}),
                        uri = URL.createObjectURL(blob),
                        _iFrame = document.createElement('iframe');
                    _iFrame.setAttribute('src', uri);
                    _iFrame.setAttribute('style', 'width: 100%; height: 87%;');
                    $('.modal-body').append(_iFrame)
                }
            };
            xhr.send();

            // let imgReq = new XMLHttpRequest();
            // imgReq.open('GET', file_assets_link, true);
            // imgReq.onreadystatechange = function (e) {
            //     if (this.status == 200) {
            //         let imgs = this.response;
            //         $('.row.gallery').append(imgs.forEach(function (img) {
            //             let curDiv = document.createElement('div');
            //             curDiv.setAttribute('class', 'col-lg-3 col-md-4 col-xs-6 thumb');
            //
            //             let curImg = document.createElement('img');
            //             curImg.setAttribute('src', 'data:image/png;base64,' + img);
            //             curImg.setAttribute('class', 'img-fluid img-thumbnail');
            //
            //             curDiv.appendChild(curImg);
            //
            //             return curDiv
            //         }))
            //     }
            // };
            // imgReq.send();

            $.get(file_assets_link, function(responseText) {
                $('.row.gallery').append(responseText.forEach(function (img) {
                    let curDiv = document.createElement('div');
                    curDiv.setAttribute('class', 'col-lg-3 col-md-4 col-xs-6 thumb');

                    let curImg = document.createElement('img');
                    curImg.setAttribute('src', 'data:image/png;base64,' + img);
                    curImg.setAttribute('class', 'img-fluid img-thumbnail');

                    curDiv.appendChild(curImg);

                    return curDiv
                }))
            });
        }
        else if(/.*ppt/.test(file_name)){
        }
        else {
            xhr.onload = function(e) {
                if (this.status == 200) {
                    // Note: .response instead of .responseText
                    var blob = new Blob([this.response], {type: 'text/plain'}),
                        uri = URL.createObjectURL(blob),
                        _iFrame = document.createElement('iframe');
                    _iFrame.setAttribute('src', uri);
                    _iFrame.setAttribute('style', 'width: 100%; height: 87%;');
                    $('.modal-body').append(_iFrame)
                }
            };
            xhr.send();
        }
    });
});


